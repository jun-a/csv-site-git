class ClientUser < ActiveRecord::Base

  #1:nテーブルの設定
  has_many :donations
    
    #メソッド全体でture or falseを返す
    def correct_password?(input_password)
      #引数として持ってきたsessionに格納されたパスワードと、 sessionのemailで検索されたユーザが持っているパスワードを比較して検証
      self.password  ==  input_password #入力されたパスワードの文字列をハッシュ化して検証 →検証のやり方はあっているのか？
      #input_passwordはもともと文字列で入ってきている
      #インスタンスメソッド(self.)の利用でuserもこっちへ持って来れる
      #パスワードはMD5化したもので、password_digeに保存 
    end

=begin
    def self.hashed_password(unique_password) #self.を入れないとnomethod error が返る
        Digest::MD5.hexdigest(unique_password).to_s
    end
=end

end