class Tag < ActiveRecord::Base
  #nnテーブル設定(tag)
  has_many :active_reports_tag
  has_many :active_reports, through: :active_reports_tag
end