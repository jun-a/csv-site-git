class ActiveReport < ActiveRecord::Base

  
  scope :available, -> {
    where('publish_start_date <= ? OR publish_start_date IS NULL', Date.today)
      .where('publish_end_date >= ? OR publish_end_date IS NULL', Date.today)
  }  

  belongs_to :category
  belongs_to :client_user
  belongs_to :client
  #nnテーブル設定(image)
  has_many :images_active_report
  has_many :images, through: :images_active_report

  #nnテーブル設定(tag)
  has_many :active_reports_tag
  has_many :tags, through: :active_reports_tag
  
  #has_and_belongs_to_many :tags 

end