class Image < ActiveRecord::Base
  has_many :images_active_report
  has_many :active_reports, through: :images_active_report
end