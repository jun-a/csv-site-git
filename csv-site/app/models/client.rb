class Client < ActiveRecord::Base
  
  include ActiveModel::ForbiddenAttributesProtection

  belongs_to :donation_purpose
  belongs_to :donation_pay_method
  belongs_to :contract_status
  belongs_to :prefectures_name

  has_many :employees
  has_many :active_reports

end