#coding: utf-8
class SentMailController < ApplicationController

=begin
  def retry_password_to_email
    @retry_password = Hash.new
    @retry_password = ClientUser.find_by(email: params[:email])
    retry_password_to_email(@retry_password) #formからわたってきたemailでユーザーを特定して結果をmailerメソッドに送る
  end
=end
#ここはgetとpostを分けてあげる必要がある
  def retry_password_to_email #get
  end

  def retry_password_to_email_post  #post
    user = ClientUser.find_by(email: params[:sent_mail][:email]) #sent_mail.rbへの引数の受け渡し
    mail = SentMail.to_retry_password_mail(user)
    mail.transport_encoding = "quoted-printable"
    mail.deliver_now   #ここでSentMailクラスのメソッドを指定してメールを送信している(メソッドの選択はどこに、どのんなメッセージを送るかの設定)
    redirect_to root_path
  end


end

#@retry_password = ClientUser.new

#出てくるのは、params[:sent_mail][:email]
#routingの設定が不可欠
