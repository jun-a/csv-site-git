class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  #ログインした場合の現在のユーザー設定
  # @loging_userが空の場合は、session情報をキーにしてDBから検索する(||=の初期化イディオムを利用)

  private
    def loging_client_user
      loging_client_user ||= ClientUser.find_by(id: session[:client_user_id]) if session[:client_user_id].present? 
    end

    def loging_admin_user
      loging_admin_user ||= AdminUser.find_by(id: session[:admin_user_id]) if session[:admin_user_id].present? 
    end

    def require_client_user_login!   #ログインして「いない」場合の判定なので、否定の!をメソッドの最後にもちいる
      if loging_client_user.blank?
        redirect_to login_client_user_login_path
        return  #メソッドが判定を決める処理をを行うため、if文の実行結果をメソッドに返してあげる
      end
    end
    
    def require_admin_user_login!   #ログインして「いない」場合の判定なので、否定の!をメソッドの最後にもちいる
      if loging_user.blank?
        redirect_to login_admin_user_login_path
        return  #メソッドが判定を決める処理をを行うため、if文の実行結果をメソッドに返してあげる
      end
    end
  
  
end
