class ActiveReportsController < ApplicationController

  before_action :require_client_user_login!, except: [:index, :show]

  def index
    @active_reports = ActiveReport.all
  end

  def show
    @active_report = ActiveReport.find(params[:id])
    #@image  = ImagesActiveReport.find(:active_report_id => @active_report.id) #active_reportsのidで探す
  end

  def new
    @active_report = ActiveReport.new
  end

  def create
    @active_report = ActiveReport.new(active_report_params)
    @active_report.save
    redirect_to active_report_path
  end

 

  private
  def active_report_params
    params.require(:active_report).permit(:title, :content, :publish_start_date, 
                                   :publish_end_date, :category_id, :client_id,
                                   :client_user_id, image_ids: [], tag_ids: [] )  #nnテーブルでひもづけている場合は、idが複数系かつ配列表示を指定してあげる 
  end
end

