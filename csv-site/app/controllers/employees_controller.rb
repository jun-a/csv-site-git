class EmployeesController < ApplicationController
  
  #nullの許可&try(:)で制御
  def show
    @employee = Employee.find(params[:id])
  end

#メンバーの検索式の実行
=begin
  def find
    @datas = []
    @word = params[:employee_find]
    employee_find = params[:employee_find].to_s
    @datas = Employee.where("name like '%" + employee_find + "%'")
    render '/employees/find_result'
  end
=end

  def new
    @employee = Employee.new
  end

  def create
    @employee = Employee.new(employee_params)
    @employee.save
    redirect_to employee_path
  end

  def edit
    @employee = Employee.find(params[:id])
  end

  def update
    @employee = Employee.find(params[:id])
    @employee.update_attributes(employee_params)
    redirect_to employee_path
  end

  private
  def employee_params
    params.require(:employee).permit(:name, :birth_day, :employee_email, 
                                     :donation_price, :date_of_hire, :client_position, :sex_id, :client_id) 
  end
end
