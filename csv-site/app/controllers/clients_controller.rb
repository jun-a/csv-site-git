class ClientsController < ApplicationController
  
  #before_action or before_filterはapplication_controllerに記載すべきか？ その場合、実行するコントローラー(class名の指定はどうするのか？)  
  before_action :require_client_user_login!, except: [:index, :show]
  before_action :require_client_user_login!, only: [:destroy]


  before_filter  :loging_client_user, only: [:edit, :destroy]


  def new
    @client = Client.new
  end

  def create
    @client = Client.new(client_params)
  
    @client.save
    render :text => 'success'
  end
  
  def show
    @client = Client.find(params[:id])
    @client_donations = Donation.where(client_id: @client.id) #引っかかるものをすべて出す
  end

  def index
    @clients = Client.all
  end

  def edit
    @client = Client.find(params[:id])
  end

  def update
    @client = Client.find(params[:id])
    @client.update_attributes(client_params)
    redirect_to client_path
  end



  private
    def client_params
      params.require(:client).permit(:name, :address, :tel_number, 
                                   :client_email, :client_president, :client_manager, :client_description, 
                                   :price_per_employee, :contract_status_id, :donation_purpose_id, 
                                   :donation_pay_method_id, :prefectures_name_id ) 
    end
end
