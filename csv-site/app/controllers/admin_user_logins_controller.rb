class AdminUserLoginsController < ApplicationController
  def new
  end

  def create
    user = AdminUser.find_by(email: params[:admin_user_logins][:email])  #検索で条件を入れるときは、find_by
    if user.correct_password?(params[:admin_user_logins][:password])  #sessionに格納されているpasswordと、sessionのemailを引数として渡す
      session[:admin_user_id] = user.id
      redirect_to root_path
    else
      render :text => 'login error'
    end
  end

  def destroy 
    session[:admin_user_id] = nil  #sessionに代入されているuser_idをnilにしてあげればok
    redirect_to root_path
  end
end
