class ClientUserLoginsController < ApplicationController

  def new
  end

  def create
    user = ClientUser.find_by(email: params[:client_user_logins][:email])  #検索で条件を入れるときは、find_by
    if user.present?
      if user.correct_password?(params[:client_user_logins][:password])  #sessionに格納されているpasswordと、sessionのemailを引数として渡す
        session[:client_user_id] = user.id
        #redirect_to client_path(user.client_id)  #nullが返る場合があるのでここはテストの時に注意
        SentMail.test.deliver_now
        redirect_to root_path #テスト用、実際は別の場所に返す
      else
        reset_password(user)  #メールからだとuserが切れてる
        #パスワード再設定のためにメールアドレスを記入する画面の表示　→そこからメソッドへ引数を渡す
        #SentMail.reset_password_to_mail(user) #別クラスからのメソッドの引用
        render 'sent_mail/retry_password_to_email'
        #render :reset_password
        # deliverメソッドを使って、メールを送信する →メール送信画面に飛ばしてあげる
        #SentMail.password_retry_mail(user).deliver  #paramsから引数を取る
      end
    else
      redirect_to root_path
    end
  end

  def destroy
    session[:client_user_id] = nil  #sessionに代入されているuser_idをnilにしてあげればok
    redirect_to root_path
  end

  private
    def reset_password(user_data)
      @reset_password = ClientUser.find_by(email: user_data.email)
      #render :reset_password_set
    end

    def reset_password_set
      @reset_password =  ClientUser.find(params[:id])
      @reset_password.update_attributes(password: params[:client_user][:password])
      redirect_to root_path
    end


end






#be7a23885e168f0a5c51bebc34f889be21d403a47ffb20201411ee023d50de23


#be7a23885e168f0a5c51bebc34f889be21d403a47ffb20201411ee023d50de23 *RubyMine-7.1.2.exe
