class SessionsController < ApplicationController
  def new
  end

  def create
    user = ClientUser.find_by(email: params[:session][:email])  #検索で条件を入れるときは、find_by
    if user.correct_password?(params[:session][:password])  #sessionに格納されているpasswordと、sessionのemailを引数として渡す
      session[:user_id] = user.id
      render :text => 'login success!'
    else
      render :text => 'login error'
    end
  end


  def destroy 
    session[:client_user_id] = nil  #sessionに代入されているuser_idをnilにしてあげればok
    redirect_to root
  end
end
