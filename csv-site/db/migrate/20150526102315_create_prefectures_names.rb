class CreatePrefecturesNames < ActiveRecord::Migration
  def change
    create_table :prefectures_names do |t|
      t.string :prefectures_name

      t.timestamps null: false
    end
  end
end
