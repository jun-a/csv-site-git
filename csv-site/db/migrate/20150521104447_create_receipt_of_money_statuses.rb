class CreateReceiptOfMoneyStatuses < ActiveRecord::Migration
  def change
    create_table :receipt_of_money_statuses do |t|
      t.boolean :receipt_of_money_status
      

      t.timestamps null: false
    end
  end
end
