Rails.application.routes.draw do


  get 'sent_mail/retry_password_to_email'

=begin
  resources :clients, path: :form, only: [] do
    get 'entry', to: :new
    post  'entry', to: :create
  end
=end

=begin
  resources :clients, path: :account, except: [] do
    get '', to: :show_account
  end
=end

  #記事系のルート設定
  resources :active_reports
  resources :images

  #登録情報系のルート設定
  resources :clients
  resources :employees

  #画像操作についてのルート設定


=begin
  get 'employee/find', to: 'employees#find'
  get 'employees/find_result'
=end

  #root_pathの設定
  root :to => 'start_page#start_page'

  #client_userとadmin_userのログイン分けを行う

  #resouses にしてしまうとrestful なルーティングに
  resource :admin_user_login, path: :admin, only: [] do
    get  'login',  to: :new
    post 'login',  to: :create
    get  'logout', to: :destroy
  end

  resource :client_user_login, path: :user, only: [] do
    get  'login',  to: :new
    post 'login',  to: :create
    get  'logout', to: :destroy
    get  'reset_password/:id', to: :reset_password, as: 'reset_password'
    patch 'reset_password/:id', to: :reset_password_set
  end

  get "sent_mail/retry_password_to_email" , to: 'sent_mail#retry_password_to_email', as: 'retry_password'
  post "sent_mail/retry_password_to_email" , to: 'sent_mail#retry_password_to_email_post'

  #match "user/reset_password" => 'client_user_login#reset_password', as: 'reset_password', via: [:get, :post]

=begin
  resource :session, path: :admin, only: [] do
    get 'login', to: :new
    post 'login', to: :create
    get 'logout', to: :destroy
  end
=end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
